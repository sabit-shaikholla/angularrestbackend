# REST API for a Mathematical Calculator on Spring Boot
This document describes the REST API for a web application that functions as a mathematical calculator.

### Entities
- Calculator: The main entity, performing various arithmetic calculations.
- Operation: Represents a mathematical operation with information like type (addition, subtraction, etc.) and operands (numbers to be operated on).
- Result: Represents the outcome of an operation, including the result value, message (success/error), and an error flag.

### Operations
The API supports basic arithmetic operations: addition, subtraction, multiplication, and division.

- HTTP Method: POST
- Endpoints: 
  - /calculator/add
  - /calculator/subtract
  - /calculator/multiply
  - /calculator/divide
- Request: An operation object containing two operands for provided operation.
- Response: The result of the provided mathematical operation.

### REST API Description
This REST API allows users to perform basic arithmetic calculations. It provides endpoints for each operation:
- /calculator/add for addition
- /calculator/subtract for subtraction
- /calculator/multiply for multiplication
- /calculator/divide for division

Each endpoint accepts a POST request with an operation object containing operands. The response includes the result of the operation.

### Functional Requirements
- Handle basic arithmetic operations.
- Validate operands and provide informative error messages.
- Return the operation result.

### Non-Functional Requirements
- Descriptive and meaningful error messages.
- Appropriate HTTP status codes (200 for success, 400 for bad requests, 500 for server errors).
- Logging for error handling and debugging.
- Secure API with protection against input validation vulnerabilities.

### Richardson Maturity Model
This API follows Level 2, utilizing HTTP methods and resources (endpoints) for operations.

### Errors
Errors are communicated using HTTP status codes and informative messages. Common errors include division by zero, invalid operands, and general server errors.

### Authentication Method
The provided code lacks authentication. Production APIs should implement appropriate authentication and authorization mechanisms.